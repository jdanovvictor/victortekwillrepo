package Worker;
import ShoppingCart.Cart;

public class Worker {
  private String name;
  private int id;
  private boolean isMarried;
  private int salary;
  private static int workersAmount;
  private static String companyName = "Blizzard";

  public Worker(String name, int id, boolean isMarried, int salary) {
    this.name = name;
    this.id = id;
    this.isMarried = isMarried;
    this.salary = salary;
    workersAmount ++;
  }


  public static String sayHello(String clientName) {
    return "Hello, " + clientName;
  }

  public static String sayGoodbye(String clientName) {
    return "See you, " + clientName;
  }

  public int increaseSalary(int newSalary) {
    salary += newSalary;
    return salary;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(short id) {
    this.id = id;
  }

  public boolean isMarried() {
    return isMarried;
  }

  public void setMarried(boolean married) {
    isMarried = married;
  }

  public int getSalary() {
    return salary;
  }

  public void setSalary(int salary) {
    this.salary = salary;
  }

  public static int getWorkersAmount() {
    return workersAmount;
  }

  public static void setCompanyName(String companyName) {
    Worker.companyName = companyName;
  }

  public static String getCompanyName() {
    return companyName;
  }

  public static void thankCustomer(){
    if(Cart.showNumberOfGamesInCart() > 2) {
      System.out.println("You are a great customer!");
    } else {
      System.out.println("Thank you , but please, buy some more games!");
    }

  }


}
