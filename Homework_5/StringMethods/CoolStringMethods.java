package StringMethods;

public class CoolStringMethods {
  public String method1(String input){
    return input.toUpperCase();
  }

  public int method2(String input){
    return input.length();
  }

  public String method3(String input){
    return input.replace("c","l");
  }

  public String method4(String input){
    return input.concat(" and books");

  }
}
