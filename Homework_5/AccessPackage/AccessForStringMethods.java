package AccessPackage;

import StringMethods.CoolStringMethods;

public class AccessForStringMethods {

  CoolStringMethods testStr = new CoolStringMethods();


  public String method1A(String text) {
    return testStr.method1(text);
  }

  public int method2A(String text) {
    return testStr.method2(text);
  }

  public String method3A(String text){
    return testStr.method3(text);
  }

  public String method4A(String text){
    return testStr.method4(text);
  }
}
