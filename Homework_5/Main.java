import AccessPackage.AccessForStringMethods;
import Worker.Worker;
import ShoppingCart.Cart;
public class Main {
  public static void main(String[] args) {
    Worker john = new Worker("John", 1, true,1000);
    System.out.println(john.getName());
    System.out.println(john.getSalary());
    john.increaseSalary(400);
    System.out.println(john.getSalary());
    System.out.println(Worker.getWorkersAmount());
    System.out.println(Worker.getCompanyName());
    Worker.setCompanyName("Valve");
    System.out.println(Worker.getCompanyName());
    Worker naruto = new Worker("Naruto", 2, false,500);
    System.out.println(Worker.getWorkersAmount());
    System.out.println(Worker.sayHello("Cooper"));
    AccessForStringMethods myClass = new AccessForStringMethods();
    System.out.println(myClass.method1A("Hello"));
    System.out.println(myClass.method2A("I love poetry"));
    System.out.println(myClass.method3A("cake"));
    System.out.println(myClass.method4A("I love music"));
    Cart.addGameToCart("Half-life 3");
    Cart.addGameToCart("Doom");
    Cart.addGameToCart("Quake");
    System.out.println(Cart.showNumberOfGamesInCart());
    Worker.thankCustomer();




  }
}
