package ShoppingCart;

import java.util.ArrayList;

public class Cart {
  private static ArrayList<String> shoppingCart = new ArrayList<String>();

  public static void addGameToCart(String gameName) {
    shoppingCart.add(gameName);
  }

  public static int showNumberOfGamesInCart(){
    return shoppingCart.size();
  }
}