public class Main {
 public static void main(String[] args) {

  sum(10);
  helloWorld(10);
  printPareNumbers(12);
  printImpareNumbers(20);
  printHelloWorldUsingDo(10);

 }


 private static void sum(int n) {
  int sum = 0;
  int start;
  for (start = 0; start <= n; start++) {
   sum += start;
  }
  System.out.println("Suma cifrelor pina la " + n);
  System.out.println(sum);
  System.out.println("-------------------");
 }

 private static void helloWorld(int n) {
  System.out.println("Printeza Hello World de " + n + " ori");
  for (int i = 1; i <= n; i++) {
   System.out.println(i + " Hello World!");
  }
  System.out.println("-------------------");

 }

 private static void printPareNumbers(int n) {
  int count = 0;
  System.out.println("Printeza pare numbers pina la " + n);
  for (int i = 0; i < n; i++) {
   if (i % 2 == 0) {
    System.out.println(i);
    count++;
   }
  }
  System.out.println("There are " + count + " numbers");
  System.out.println("-------------------");

 }

 private static void printImpareNumbers(int n) {
  int count = 0;
  System.out.println("Printeza impare numbers pina la " + n);
  for (int i = 1; i < n; i++) {
   if (i % 2 != 0) {
    System.out.println(i);
    count++;
   }

  }
  System.out.println("There are " + count + " numbers");

 }

 public static void printHelloWorldUsingDo(int n) {
  System.out.println("---------------");
  int i = 1;
  do {
   System.out.println(i + " Hello World");
   i++;

  } while (i <= n);
 }
}