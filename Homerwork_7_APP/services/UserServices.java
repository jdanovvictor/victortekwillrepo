/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reigisterapp.services;

import reigisterapp.dao.UserDao;
import reigisterapp.model.User;

/**
 *
 * @author User
 */
public class UserServices {
    public static UserDao dao = new UserDao();

    public void add(User user, int index){
        dao.add(user,index);
    }

    public void remove(int index){
        dao.remove(index);
    }

    public void update(int index, String username){
        dao.update(index,username);
    }

    public int getID(int index){
        return dao.getID(index);
    }

    public String getUserName(int index){
        return dao.getUserName(index);
    }
}
