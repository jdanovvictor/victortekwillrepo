/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reigisterapp.dao;

import java.util.Arrays;
import reigisterapp.model.User;

/**
 *
 * @author User
 */
public class UserDao {
    public User userDb[] = new User[10];

    public int getIndex(){
        for(int i = 0; i < userDb.length; i++){
            if(userDb[i] == null) {
                return i;
            }
        }
        return userDb.length;
    }

    public void add(User user, int index){
        userDb[index] = user;
     }

     public void remove(int index){
        userDb[index] = null;
     }

     public void removeArray(){
         Arrays.fill(userDb, null);

     }
    public void update(int index, String username) {
        userDb[index].setUsername(username);

    }


    public int getID(int index) {
        return userDb[index].getId();
    }

    public String getUserName(int index) {
        return userDb[index].getUsername();
    }


    public void showDb() {
        for(User user: userDb) {
            if(user != null) {
                System.out.println(user);
            }
        }
        System.out.println();
    }
    }
