/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reigisterapp.model;

/**
 *
 * @author User
 */
public class User {
    private String username;
    private int id;

    public void setId(int id){
        this.id = id;
    }
    public void setUsername(String username){
        this.username = username;
    }

    public String getUsername(){
        return this.username;
    }

    public int getId(){
        return this.id;
    }

    @Override
    public String toString() {
        return "User{"+ "ID=" + id + ", username=" + username +  '}';
    }
}
