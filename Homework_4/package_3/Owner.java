package package_3;

public class Owner {
  String name;
  short clientID;

  public Owner(String name, short clientID) {
    this.name = name;
    this.clientID = clientID;
  }

  private void showOwnerInfo() {
    System.out.println(name + " " + clientID);
  }

  private String returnSomeString() {
    return "the string";
  }

  // putting 2 private methods into public method to call them in another class
  public void callingPrivateMethods() {
    showOwnerInfo();
    String someString = returnSomeString();
    System.out.println(someString);
  }
}
