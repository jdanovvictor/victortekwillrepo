package package_2;

public class Dog {
  int age;
  String name;
  String breed;

  public Dog(int age, String name, String breed) {
    this.age = age;
    this.name = name;
    this.breed = breed;
  }


  private void showInfo() {
    System.out.println(age);
    System.out.println(name);
    System.out.println(breed);
  }

  private void printInt() {
    System.out.println(age);
  }

  void printChar(){
    char x = 'k';
    System.out.println(x);
  }


  // putting 2 private methods and one default into public method to call them in another class
  public void publicMethod() {
    showInfo();
    printInt();
    printChar();
    System.out.println("Public method called");
  }
}
