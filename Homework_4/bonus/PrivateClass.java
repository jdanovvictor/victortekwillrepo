package bonus;

// the task was done using getters and setters
public class PrivateClass {
  private int a;
  private boolean isBoolean;

  public PrivateClass(int a, boolean isBoolean) {
    this.a = a;
    this.isBoolean = isBoolean;
  }

  public int getA() {
    return a;
  }

  public boolean getBoolean() {
    return isBoolean;
  }

  public void setA(int a){
    this.a = a;
  }

  public void setBoolean(boolean isBoolean){
    this.isBoolean = isBoolean;
  }
}
