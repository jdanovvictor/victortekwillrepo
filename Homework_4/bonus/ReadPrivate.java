package bonus;

public class ReadPrivate {
  public static void main(String[] args) {
    PrivateClass class1 = new PrivateClass(1, false);
    System.out.println(class1);
    System.out.println(class1.getA());
    System.out.println(class1.getBoolean());
    class1.setA(2);
    System.out.println(class1.getA());
    class1.setBoolean(true);
    System.out.println(class1.getBoolean());
  }
}
