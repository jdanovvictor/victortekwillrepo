package package_1;
import package_2.Dog;
import package_3.Owner;


public class Petshop {
  public static void main(String[] args) {
  Dog pitbull = new Dog(2,"Gerald","Pitbull");
  pitbull.publicMethod();
  Owner john = new Owner("John", (short) 1);
  john.callingPrivateMethods();

  }
}
