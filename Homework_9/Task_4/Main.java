package Homework_9.Task_4;
import java.util.*;

public class Main {
  public static void main(String[] args) {
    ArrayList<Integer> myArr = new ArrayList<>(5);
    myArr.add(2);
    myArr.add(6);
    myArr.add(10);
//    myArr.clear();
    myArr.remove(2);

    for(Integer x : myArr){
      System.out.println(x);
    }

    LinkedList<String> names = new LinkedList<>();
    names.add("Tom");
    names.add("James");
    names.add(1, "Alex");

    for(String x : names){
      System.out.println(x);
    }

    List<String> list = new ArrayList<>();
    list.add("var1");
    list.get(0);
    list.remove(0);
    for(String item : list) {
      System.out.println(item);
    }

    HashSet<Integer> myHashSet = new HashSet<Integer>();
    myHashSet.add(1);
    myHashSet.add(2);
    myHashSet.add(3);
    myHashSet.isEmpty();

    for (int i : myHashSet)
      System.out.println(i);
  }

  }

