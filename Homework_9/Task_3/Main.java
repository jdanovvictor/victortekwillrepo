package Homework_9.Task_3;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
      try {
        int result = divideInt(10,5);
        System.out.println("10 divided by 5 is " + result);
        divideInt(10,0);
        System.out.println("10 divided by 0 is " + result);
      }

      catch (MyException ex){
        System.out.println(ex.getMessage());
      }
  }

  public static int divideInt(int i, int j) throws MyException{
    if(j == 0){
      throw new MyException("Divisor cannot be zero");
    }
    return i/j;
  }

}
