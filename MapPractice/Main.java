package MapPractice;


import java.util.HashMap;
import java.util.Map;

public class Main {
  public static void main(String[] args) {
    Map<Integer, Student> studentMap = new HashMap<>();
    Student student1 = new Student("Alex");
    Student student2 = new Student("Serj");
    studentMap.put(1, student1);
    studentMap.put(2, student2);
    System.out.println(studentMap);
    System.out.println(studentMap.size());
  }
}
