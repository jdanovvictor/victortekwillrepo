package Homework_7.Task_1;

public class Wizard extends NPC {
  String className;
  int mana;

  Wizard(String name, int hp, String className, int mana){
    super(name,hp);
    this.mana = mana;
  }

  @Override
  void attack(){
    System.out.println("The great wizard " + name + " spells a fireball using his Spellbook!");
  }

  // final method can not be override in child
//  void testFinal(){
//
//  }
}
