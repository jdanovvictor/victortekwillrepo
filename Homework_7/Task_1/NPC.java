package Homework_7.Task_1;

public class NPC {
  String name;
  int hp;

  NPC(){}

  NPC(String name, int hp){
    this.name = name;
    this.hp = hp;
  }

  void attack(){
    System.out.println(name + " attacks with his bare hands!");
  }

  final void testFinal(){
    System.out.println("Final method");
  }
}
