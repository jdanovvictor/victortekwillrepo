package Homework_7.Task_3;
import java.util.Arrays;

class ArrayMethod {
  static void reverseArray(int arr[]) {
    System.out.println("Array before reverse " + Arrays.toString(arr));

    for (int i = 0; i < arr.length / 2; i++) {
      int tempArray = arr[i];
      arr[i] = arr[arr.length - i - 1];
      arr[arr.length - i - 1] = tempArray;
    }
    System.out.println("Array after reverse: " + Arrays.toString(arr));
  }
}


public class Reverse {
  public static void main(String[] args) {
    int[] numbers = {1, 2, 3, 4, 5};
    ArrayMethod.reverseArray(numbers);
  }
}
