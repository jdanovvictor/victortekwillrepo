package Homework_7.Task_2;

public class IndexMethod {
  static void findIndex(int element, int arr[]){
    for(int i = 0; i < arr.length; i++){
      if(arr[i] == element){
        System.out.println("Index position of " + element + " is: " + i);
      }
    }
  }
}

class Test{
  public static void main(String[] args) {
    int arr[] = {1,2,3,4,5};
    IndexMethod.findIndex(2, arr);
  }
}
