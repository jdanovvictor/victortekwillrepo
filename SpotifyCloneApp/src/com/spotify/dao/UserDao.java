/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spotify.dao;

import com.spotify.model.User;
import java.util.HashMap;

/**
 *
 * @author User
 */
public interface UserDao {
   public static HashMap<Integer,User> UserDB = new HashMap<Integer,User>();
   public boolean create(User user);
   public boolean update(User user);

   public User findById(int id);
   public void findAll();
}
