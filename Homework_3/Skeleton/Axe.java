public class Axe {
    String material;
    boolean isBroken;
    public Axe (String material, boolean isBroken){
        this.material = material;
        this.isBroken = isBroken;
    }


    void checkAxe(){
        System.out.println("My axe is made of " + material + "!");
    }
}