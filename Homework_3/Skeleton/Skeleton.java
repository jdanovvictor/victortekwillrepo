public class Skeleton {
    int hp;
    String name;
    int attackDmg;
    boolean isAlive;
    Axe axe;

    public Skeleton(int hp, String name, int attackDmg, boolean isAlive) {
        this.hp = hp;
        this.name = name;
        this.attackDmg = attackDmg;
        this.isAlive = isAlive;
    }

    int drinkPotion(int buff){
        hp += buff;
        System.out.println(name + " drank hp potion!");
        return hp;
    }

    void npcAppear() {
        System.out.println("NPC_Skeleton_Name: "  + name);
        System.out.println("Skeleton_attack: " + attackDmg);
        System.out.println("Skeleton_hp: " + hp);
    }

     void  checkIfAlive() {
         if (hp > 0) {
             System.out.println("Is alive!");
         } else {
             System.out.println("Is Dead!");
         }
     }
        Skeleton makeRandomSkeleton(){
            Skeleton skeleton2 = new Skeleton(24,"Nick",12, true);
            return skeleton2;
         }



}