public class Car {
  int deliveryPrice;
  String brand;
  int price;
  boolean isUsed;
  int finalPrice;


  // constructor when all parameters defines
  Car(String brand, int price, boolean isUsed) {
    this.brand = brand;
    this.price = price;
    this.isUsed = isUsed;
  }

  // constructor when no parameters defined
  Car() {
    brand = "BMW";
    price = 3000;
    isUsed = true;
  }


  void showCarInfo() {
    System.out.println("--------------------");
    System.out.println("Brand: " + brand);
    System.out.println("Price: " + price);
    System.out.println("Is used before: " + isUsed);
  }

  void increasePrice(int increaseValue) {
    price += increaseValue;
  }

  void makeOrder(String orderType) {
    switch (orderType) {
      case "Standart":
        deliveryPrice = 300;
        System.out.println("Order type is standart, be careful, but not so fast");
        break;
      case "Express":
        deliveryPrice = 500;
        System.out.println("Order type is Express, be careful and fast!");
        break;
      default:
        System.out.println("The type of delivery is wrong");
    }
    finalPrice = price + deliveryPrice;
    System.out.println("Final price is " + finalPrice);
  }

  int makeDiscount(int discountAmount) {
    return finalPrice - discountAmount;
  }
}
