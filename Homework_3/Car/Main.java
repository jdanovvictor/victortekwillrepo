public class Main {
  public static void main(String[] args) {
    Car hondacrv = new Car("Honda", 5000, true);
    hondacrv.showCarInfo();
    hondacrv.increasePrice(300);
    hondacrv.showCarInfo();
    hondacrv.makeOrder("Express");
    System.out.println(hondacrv.makeDiscount(150));
    Car bmw1 = new Car();
    bmw1.showCarInfo();
    Motobike moto1 = new Motobike();
    moto1.showCarInfo();

  }
}
