package HashMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
//        int a = 10;
//        int b  = 3;
//        int c = 88;
//        HashMap<String, Integer> happy = new HashMap<String, Integer>();
//        happy.put("a", 10);
//        happy.put("b", 3);
//        happy.put("c", 88);
//        System.out.println(happy);
        HashMap<Integer, String> passportsAndNames = new HashMap<>();
        HashMap<Integer, String> passportsAndNames2 = new HashMap<>();

        passportsAndNames.put(212121, "Гаврилов Иван");
        passportsAndNames.put(213151, "Панин Алексей");
        passportsAndNames.put(287451, "Бояршинов Николай");
        passportsAndNames2.put(917352, "Алексей Андреевич Ермаков");
        passportsAndNames2.put(925648, "Максим Олегович Архаров");
        System.out.println(passportsAndNames);
        String getName = passportsAndNames.get(212121);
        System.out.println(getName);
        passportsAndNames.remove(287451);
        System.out.println(passportsAndNames);
        Set<Integer> keys = passportsAndNames.keySet();
        System.out.println("Ключи: " + keys);

        ArrayList<String> values = new ArrayList<>(passportsAndNames.values());
        System.out.println("Значения: " + values);
        System.out.println(passportsAndNames.isEmpty());
        passportsAndNames.putAll(passportsAndNames2);
        System.out.println(passportsAndNames);



    }
}
