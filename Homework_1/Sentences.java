class Sentences {
	public static void main(String [] args) {
		System.out.print("Java is one of many programming languages and technologies supported by Stackify’s leading tools, Retrace and Prefix. ");
		System.out.print("Because at Stackify we aim to help developers become better developers, we’re taking a look at some of the foundational concepts in the Java programming language. ");
		System.out.print("OOP concepts in Java are the main ideas behind Java’s Object Oriented Programming.");
		return;
	}
}