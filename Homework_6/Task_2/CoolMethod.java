package Homework_6.Task_2;

public class CoolMethod {
  public int sum(int x, int y){
    return x + y;
  }

  public int sum(int x, int y, int z){
    return  x + y + z;
  }

  public float sum (float x, float y){
    return x + y;
  }
}
