package Homework_6.Task_6;

public class NPC {
  String name;
  String npcClass;
  int hp;

  NPC(String name, String npcClass, int hp){
    this.name = name;
    this.npcClass = npcClass;
    this.hp = hp;
  }

  private void display(NPC object){
    System.out.println("***********");
    System.out.println("NPC name: " + object.name);
    System.out.println("Class: " + object.npcClass);
    System.out.println("Health: " + object.hp);
  }

  public NPC resetNPC(){
    this.name = "Default_Name";
    this.npcClass = "Default_Class";
    this.hp = 0;
    return this;
  }

  public void getter(){
    display(this);
  }
}

class Test{
  public static void main(String[] args) {
    NPC knight1 = new NPC("Richard","knight",123);
    knight1.getter();
    knight1.resetNPC();
    knight1.getter();


  }
}
