package Homework_6.Task_7;

public class NPC {
  String name;
  String npcClass;
  int hp;

  static {
    System.out.println("Static block executed");
    System.out.println("New NPC created!");
  }

  {
    this.name = "Default";
    this.npcClass = "Default";
    this.hp = 0;
    System.out.println("Non-static block executed");
  }

  NPC(){}

  NPC(String name, String npcClass, int hp){
    this.name = name;
    this.npcClass = npcClass;
    this.hp = hp;
  }

  private void display(NPC object){
    System.out.println("***********");
    System.out.println("NPC name: " + object.name);
    System.out.println("Class: " + object.npcClass);
    System.out.println("Health: " + object.hp);
  }

  public void getter(){
    display(this);
  }
}


class Test{
  public static void main(String[] args) {
        NPC defaultNPC = new NPC();
        defaultNPC.getter();
        NPC witch1 = new NPC("Kira", "Witch", 100);
        witch1.getter();
  }
}