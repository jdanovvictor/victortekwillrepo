package Homework_8.Task_5;

public class PizzaCoocker implements NeapolitanaInterface, VegetarianInterface {
  @Override
  public void addCheese() {
    System.out.println("adding cheese");
  }

  @Override
  public void addMeat(){
    System.out.println("adding meat");
  }


  @Override
  public void addVegetables(){
    System.out.println("adding vegetables..");
  }
}
