package Homework_8.Task_4;

public interface VendingMachineInterface {
  public void getMoney(int money);
  public void powerOn();
  public void powerOff();
  public void giveItem(String item);
}
