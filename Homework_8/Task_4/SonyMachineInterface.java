package Homework_8.Task_4;

public interface SonyMachineInterface extends VendingMachineInterface {
  public void giveRest(int givenMoney);
  public void setNewPrices(int itemID, int newPrice);
  public void hardReset();
}
