package Homework_8.Task_4;

public interface ToshibaMachineInterface extends VendingMachineInterface {
  public void resetSelection(int selection);
  public void addNewItem(int itemID, int price);

}
