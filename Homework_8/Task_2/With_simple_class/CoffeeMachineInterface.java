package Homework_8.Task_2.With_simple_class;

public interface CoffeeMachineInterface {
  public abstract void makeCoffee();
  public abstract void addSugar();
  public abstract void addMilk();

}
