package Homework_8.Task_2.With_abstract_class;

public abstract class Sony implements CoffeeMachineInterface {
  @Override
  public void makeCoffee(){
    System.out.println("Making coffee....");
  }
}
