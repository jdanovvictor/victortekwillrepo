/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package md.tekwill.service;

import md.tekwill.dao.EmployeeDao;
import md.tekwill.models.Employee;

/**
 *
 * @author User
 */
public class EmployeeService {
    public static EmployeeDao dao = new EmployeeDao();
    
   public void create(Employee employee){
       dao.create(employee);
   }
   
   public void findById(int id){
       dao.findById(id);
   }
   
   public void findAll(){
       dao.findAll();
   }
    
}
