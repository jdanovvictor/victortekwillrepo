/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package md.tekwill.service;

import md.tekwill.models.Employee;

/**
 *
 * @author adragutan
 */
public interface EmployeeService {

    public boolean update(Employee employee);

    public boolean create(Employee employee);

    public Employee findById(int id);

    public void findAll();
}
